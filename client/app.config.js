
(function () {
   angular
       .module("GroceryApp")
       .config(groceryAppConfig);
    groceryAppConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function groceryAppConfig($stateProvider, $urlRouterProvider){

        $stateProvider
            .state('A',{
                url: '/A',
                templateUrl: './search/search.html',
                controller: 'SearchCtrl',
                controllerAs: 'ctrl'
            })
            .state('B',{
                url: '/B',
                templateUrl: './show/show.html',
                controller: 'ShowCtrl',
                controllerAs: 'ctrl'
            })
            .state('B1',{
                url: '/B1/:id',
                templateUrl: './show/show.html',
                controller: 'ShowCtrl',
                controllerAs: 'ctrl'
            });

        $urlRouterProvider.otherwise("/A");
    }
})();