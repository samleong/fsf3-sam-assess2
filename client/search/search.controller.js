(function () {
    angular
        .module("GroceryApp")
        .controller("SearchCtrl", SearchCtrl);

    SearchCtrl.$inject = ["$http","$state"];

    function SearchCtrl($http, $state) {
        var vm = this;

        vm.searchField = '';
        vm.result = null;

        // Exposed functions
        vm.search = search;
        vm.searchForBrand = searchForBrand;

        // Initial calls
        init();


        // Function definitions
        function init() {
            $http
                .get("/api/groceries")
                .then(function (response) {
                    vm.groceries = response.data;
                })
                .catch(function (err) {
                    console.log("error " + err);
                });
        }

        function search() {
            $http
                .get("/api/groceries/name",
                    {
                        params: {
                            'name': vm.searchField
                        }
                    }
                )
                .then(function (response) {
                    vm.groceries = response.data;
                })
                .catch(function (err) {
                    console.info("Error " + err);
                });
        }

        function searchForBrand() {
            $http.get("/api/groceries/brand",
                {
                    params: {
                        'brand': vm.searchField
                    }
                })
                .then(function (response) {
                    vm.groceries = response.data;
                })
                .catch(function (err) {
                    console.info("Error " + err);
                });
        }

        vm.showGrocery=function (mygroceryId) {
            var clickedGroceryId=mygroceryId;
            $state.go("B1",{id : clickedGroceryId});
        }
    }

})();