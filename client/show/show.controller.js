(function () {
    'use strict';
    angular
        .module("GroceryApp")
        .controller("ShowCtrl", ShowCtrl);

    ShowCtrl.$inject = ["$http", "$filter", "$stateParams"];
    function ShowCtrl($http, $filter, $stateParams) {
        console.log("-- in show.controller.js");
        // Exposed variables
        var vm = this;

        vm.id = "";
        vm.result = {};

        // Exposed functions
        vm.toggleEditor = toggleEditor;
        vm.initDetails = initDetails;
        vm.save = save;
        vm.search = search;

        // Initial calls
        initDetails();

        // Initializes grocery details shown in view
        function initDetails() {
            console.log("-- show.controller.js > initDetails()");
            vm.result.id = "";
            vm.result.name = "";
            vm.result.brand = "";
            vm.result.upc12 = "";
            vm.showDetails = false;
            vm.isEditorOn = false;

        }

        // Saves edited grocery name
        function save() {
            console.log("-- show.controller.js > save()");
            $http.put("/api/groceries/" + vm.id,
                {
                    id: vm.result.id,
                    name: vm.result.name,
                    brand: vm.result.brand,
                    upc12: vm.result.upc12
                })
                .then(function (response) {
                    console.log("-- show.controller.js > save() > results: \n" + JSON.stringify(response.data));
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > save() > error: \n" + JSON.stringify(err));
                });
            vm.toggleEditor();
        }

        // Given a department number, this function searches the Employees database for
        // the department name, and the latest department manager's id/name and tenure
        function search() {
            console.log("-- show.controller.js > search()");
            initDetails();
            vm.showDetails = true;

            $http.get("/api/groceries/" + vm.id)

                .then(function (response) {
                    // Show table structure
                    vm.showDetails = true;

                    // This is a good way to understand the type of results you're getting
                    console.log("-- show.controller.js > search() > results: \n" + JSON.stringify(response.data));

                    // Exit .then() if response data is empty
                    if(!response.data)
                        return;

                    // The response is an array of objects that contain only 1 object
                    // We are assigning value like so, so that we don't have to do access complex structures
                    // from the view. Also this would give you a good sense of the structure returned.
                    // You could, of course, massage data from the back end so that you get a simpler structure
                    vm.result.id = response.data.id;
                    vm.result.name = response.data.name;
                    vm.result.brand = response.data.brand;
                    vm.result.upc12 = response.data.upc12;
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > search() > error: \n" + JSON.stringify(err));
                });

        }

        if($stateParams.id){
            vm.id=$stateParams.id;
            vm.search();
        }
        // Switches editor state of the department name input/edit field
        function toggleEditor() {
            vm.isEditorOn = !(vm.isEditorOn);
        }
    }
})();