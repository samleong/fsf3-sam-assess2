
module.exports = function (conn, Sequelize) {
    var Grocery = conn.define("grocery_list", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        upc12: {
            type: Sequelize.BIGINT
        },
        brand: {
            type: Sequelize.STRING
        },
        name: {
            type: Sequelize.STRING
        }
    }, {
        timestamps: false
    });

    return Grocery;
};