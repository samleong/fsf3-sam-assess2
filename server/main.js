var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var Sequelize = require("sequelize");


app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(express.static(__dirname + "/../client/"));

var MYSQL_USERNAME = 'root';
var MYSQL_PASSWORD = 'firehose33';
var conn = new Sequelize('grocery_list', MYSQL_USERNAME, MYSQL_PASSWORD, {});

var Grocery = require('./models/grocery')(conn, Sequelize);

app.get("/api/groceries", function (req, res) {
    var where = {};

    if (req.query.name) {
        where.name = {
            $like: "%" + req.query.name + "%"
        }
    }

    if (req.query.id) {
        where.id = req.query.id;
    }

    console.log("where: " + JSON.stringify(where));
    Grocery
        .findAll({
            where: where,
            limit: 20,
            order: 'name'
        })
        .then(function (groceries) {
            res.json(groceries);
        })
        .catch(function () {
            res
                .status(500)
                .json({error: true})
        });

});

// update grocery in show page
app.put('/api/groceries/:id', function (req, res) {
    console.log("-- PUT /api/groceries/:id");
    console.log("-- PUT /api/groceries/:id request params: " + JSON.stringify(req.params));
    console.log("-- PUT /api/groceries/:id request body: " + JSON.stringify(req.body));

    var where = {};
    where.id = req.params.id;

    // Updates grocery detail
    Grocery
        .update(
            {
                name: req.body.name,
                brand: req.body.brand,
                upc12: req.body.upc12
            }
            , {where: where}
        )
        .then(function (response) {
            //console.log("-- PUT /api/groceries/:id grocery.update then(): \n"
              //  + JSON.stringify(response));
            console.log("done")
        })
        .catch(function (err) {
            console.log("-- PUT /api/groceries/:id grocery.update catch(): \n"
                + JSON.stringify(err));
        });

});

// search by grocery name in search page
app.get("/api/groceries/name", function (req, res) {
    var where = {};
    console.log("req.query.name = ", req.query.name);
    if (req.query.name) {
        where.name = {
            $like: "%" + req.query.name + "%"
        }
    }

    if (req.query.id) {
        where.id = req.query.id;
    }

    console.log("where: " + JSON.stringify(where));
    Grocery
        .findAll({
            where: where
        })
        .then(function (groceries) {
            res.json(groceries);
        })
        .catch(function () {
            res
                .status(500)
                .json({error: true})
        });

});

// search by grocery brand in search page
app.get("/api/groceries/brand", function (req, res) {
    var where = {};

    if (req.query.brand) {
        where.brand = {
            $like: "%" + req.query.brand + "%"
        }
    }

    if (req.query.id) {
        where.id = req.query.id;
    }

    console.log("where: " + JSON.stringify(where));
    Grocery
        .findAll({
            where: where
        })
        .then(function (groceries) {
            res.json(groceries);
        })
        .catch(function () {
            res
                .status(500)
                .json({error: true})
        });

});


// search by grocery id in show page
app.get("/api/groceries/:id", function (req, res) {
    console.log("-- GET /api/groceries/:id request params: " + JSON.stringify(req.params));

    var where = {};
    if (req.params.id) {
        where.id = req.params.id;
    }
    Grocery
        .findOne({
            where: where
        })
        .then(function (groceries) {
            console.log("-- GET /api/groceries/:id findOne then() result \n " + JSON.stringify(groceries));
            res.json(groceries);
        })
        // this .catch() handles erroneous findAll operation
        .catch(function (err) {
            console.log("-- GET /api/groceries/:id findOne catch() \n " + JSON.stringify(err));
            res
                .status(500)
                .json({error: true});
        });

});


//Start the web server on port 3000
    app.listen(3000, function () {
        console.info("Webserver started on port 3000");
    });